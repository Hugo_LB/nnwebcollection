import { HttpClient, HttpErrorResponse } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { Dataset } from 'src/app/models/dataset/dataset';


@Injectable()
export class ApiDatasetComponent {

  constructor(private httpClient: HttpClient) { }

  getDatasets(): Observable<Dataset[]> {
    return this.httpClient.get<Dataset[]>('http://localhost:5002/datasets')
  }
}
