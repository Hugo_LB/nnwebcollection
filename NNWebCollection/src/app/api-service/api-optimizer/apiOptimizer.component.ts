import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { catchError, map, Observable, of } from 'rxjs';
import { Optimizer } from 'src/app/models/optimizer/optimizer';


@Injectable()
export class ApiOptimizerComponent {

  constructor(private httpClient: HttpClient) { }

  getOptimizers(): Observable<Optimizer[]> {
    return this.httpClient.get<Optimizer[]>('http://localhost:5002/optimizers').pipe(
     map( optimizers => optimizers.map(optimizersJson => new Optimizer(optimizersJson)))
    )
  }
}