import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { MLPComponent } from './models/mlp/mlp.component';
import { RNNComponent } from './models/rnn/rnn.component';
import { ModelsComponent } from './models/models.component';
import { CNNComponent } from './models/cnn/cnn.component';
import { TransformerComponent } from './models/transformer/transformer.component';
import { LanguageHeaderComponent } from './language-header/language-header.component';


const routes: Routes = [
  { path: '', component: ModelsComponent, pathMatch: 'full'},
  { path: 'mlp', component: MLPComponent },
  { path: 'rnn', component: RNNComponent },
  { path: 'cnn', component: CNNComponent },
  { path: 'transformer', component: TransformerComponent }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
