import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { MatButtonModule } from '@angular/material/button';
import { MatGridListModule } from '@angular/material/grid-list';
import { MatSelectModule } from '@angular/material/select';
import { MatCardModule } from '@angular/material/card';
import { MatDividerModule } from '@angular/material/divider';
import { MatButtonToggleModule } from '@angular/material/button-toggle';
import { HttpClientModule } from '@angular/common/http';
import { MatTabsModule } from '@angular/material/tabs';
import { MatInputModule } from '@angular/material/input';
import { MatIconModule } from '@angular/material/icon';

import { AppComponent } from './app.component';
import { ModelsComponent } from './models/models.component';
import { AppRoutingModule } from './app-routing.module';
import { MLPComponent } from './models/mlp/mlp.component';
import { RNNComponent } from './models/rnn/rnn.component';
import { CNNComponent } from './models/cnn/cnn.component';
import { TransformerComponent } from './models/transformer/transformer.component';
import { LanguageHeaderComponent } from './language-header/language-header.component';
import { ApiDatasetComponent } from './api-service/api-dataset/apiDataset.component';
import { ApiOptimizerComponent } from './api-service/api-optimizer/apiOptimizer.component';

@NgModule({
  declarations: [
    AppComponent,
    ModelsComponent,
    MLPComponent,
    RNNComponent,
    CNNComponent,
    TransformerComponent,
    LanguageHeaderComponent,
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    MatButtonModule,
    MatGridListModule,
    MatSelectModule,
    MatCardModule,
    MatDividerModule,
    MatButtonToggleModule,
    HttpClientModule,
    MatTabsModule,
    MatInputModule,
    MatIconModule,
    FormsModule
  ],
  exports: [
    MatButtonModule,
    MatSelectModule
  ],
  providers: [ApiDatasetComponent, ApiOptimizerComponent],
  bootstrap: [AppComponent]
})
export class AppModule { }
