import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-language-header',
  templateUrl: './language-header.component.html',
  styleUrls: ['./language-header.component.css']
})
export class LanguageHeaderComponent implements OnInit {
  selected = 'french';

  constructor(public router: Router) { }

  ngOnInit(): void {
  }

}
