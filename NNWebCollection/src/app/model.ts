export interface Model {
  name: string;
  description: string;
  routing: string;
}
