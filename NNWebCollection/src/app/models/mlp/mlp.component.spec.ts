import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MLPComponent } from './mlp.component';

describe('MLPComponent', () => {
  let component: MLPComponent;
  let fixture: ComponentFixture<MLPComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MLPComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MLPComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
