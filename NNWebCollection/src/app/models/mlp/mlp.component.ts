import { Component, OnDestroy, OnInit, ViewEncapsulation } from '@angular/core';
import { ApiDatasetComponent } from 'src/app/api-service/api-dataset/apiDataset.component';
import { ApiOptimizerComponent } from 'src/app/api-service/api-optimizer/apiOptimizer.component';
import { Dataset } from '../dataset/dataset';
import { Optimizer } from '../optimizer/optimizer';

@Component({
  selector: 'app-mlp',
  templateUrl: './mlp.component.html',
  styleUrls: ['./mlp.component.css']
})
export class MLPComponent implements OnInit, OnDestroy {
  mlp_title = 'Perceptron multicouche';
  datasetsList?: Dataset[];
  optimizersList?: Optimizer[];
  layersValue = 10;
  lrValue = 0.05;
  epochValue = 1000;
  batchValue = 100;

  constructor(private datasetApi : ApiDatasetComponent, private optimizerApi : ApiOptimizerComponent) { }

  ngOnInit() {
    this.datasetApi.getDatasets().subscribe((datasets : Dataset[]) => {
      this.datasetsList = datasets;
    })
    this.optimizerApi.getOptimizers().subscribe((optimizers : Optimizer[]) => {
      this.optimizersList = optimizers;
    })
  }

  ngOnDestroy() {}

}
