import { Component, OnInit } from '@angular/core';
import { Model } from '../model';
import { Router } from '@angular/router';

@Component({
  selector: 'app-models',
  templateUrl: './models.component.html',
  styleUrls: ['./models.component.css']
})
export class ModelsComponent implements OnInit {
  models: Model[] = [
    {
      name: 'Perceptron multicouche',
      description: 'Le perceptron multicouche est un type de réseau neuronal artificiel organisé en plusieurs couches au sein desquelles une information circule de la couche d\'entrée vers la couche de sortie uniquement. Il s\'agit donc d\'un réseau à propagation directe. Chaque couche est constituée d\'un nombre variable de neurones, les neurones de la dernière couche (dite « de sortie ») étant les sorties du système global.',
      routing: '/mlp'
    },
    {
      name: 'Réseau de neurones récurrents',
      description: 'Un réseau de neurones récurrents est un réseau de neurones artificiels présentant des connexions récurrentes. Un réseau de neurones récurrents est constitué d\'unités interconnectées interagissant non-linéairement et pour lequel il existe au moins un cycle dans la structure. Les unités sont reliées par des arcs (synapses) qui possèdent un poids. La sortie d\'un neurone est une combinaison non linéaire de ses entrées.',
      routing: '/rnn'
    },
    {
      name: 'Réseau neuronal convolutif',
      description: 'En apprentissage automatique, un réseau de neurones convolutifs ou réseau de neurones à convolution est un type de réseau de neurones artificiels acycliques, dans lequel le motif de connexion entre les neurones est inspiré par le cortex visuel des animaux. Les neurones de cette région du cerveau sont arrangés de sorte qu\'ils correspondent à des régions qui se chevauchent lors du pavage du champ visuel. Leur fonctionnement est inspiré par les processus biologiques, ils consistent en un empilage multicouche de perceptrons, dont le but est de prétraiter de petites quantités d\'informations. Les réseaux neuronaux convolutifs ont de larges applications dans la reconnaissance d\'image et vidéo, les systèmes de recommandation et le traitement du langage naturel.',
      routing: '/cnn'
    },
    {
      name: 'Transformeur',
      description: 'Le transformeur (ou modèle auto-attentif) est un modèle d\'apprentissage profond introduit en 2017, utilisé principalement dans le domaine du traitement automatique des langues (TAL).\nÀ l\'instar des réseaux de neurones récurrents (RNN), les transformeurs sont conçus pour gérer des données séquentielles, telles que le langage naturel, pour des tâches telles que la traduction et la synthèse de texte. Cependant, contrairement aux RNN, les transformeurs n\'exigent pas que les données séquentielles soient traitées dans l\'ordre. Par exemple, si les données d\'entrée sont une phrase en langage naturel, le transformeur n\'a pas besoin d\'en traiter le début avant la fin. Grâce à cette fonctionnalité, le transformeur permet une parallélisation beaucoup plus importante que les RNN et donc des temps d\'entraînement réduits.',
      routing: '/transformer'
    }
  ];
  

  constructor( 
    private router: Router,
  ) { }

  ngOnInit() {
  }
}
