import { Parameter } from "./parameter";

export class Optimizer {
    public name: string;
    public parameters: Parameter[];

    constructor(data: { 
      name: string; 
      parameters: {
        name: string; 
        length: number; 
        type: string; 
        defaultValue: string;
      }[]
     }) { 
      this.name = data.name;
      this.parameters = data.parameters.map(parameter => new Parameter(parameter))
    }
  
  }