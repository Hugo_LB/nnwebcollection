export class Parameter {
    public name: string;
    public defaultValue: any;
    public type: string;
    public length: number;
    
    constructor(data: { name: string; length: any; type: string; defaultValue: string; }) {
      this.name = data.name;
      this.length = Number(data.length);
      if(data.type === 'tuple'){
        this.type = "Array<number>"
        var newDefaultValue = data.defaultValue.replace("(","");
        newDefaultValue = newDefaultValue.replace(")","");
        this.defaultValue = newDefaultValue.split(",").map(Number);
        
      } else if(data.type === "float" || data.type === "int") {
        this.type = "number";
        this.defaultValue = Number(data.defaultValue)
      } else if(data.type === "bool"){
        this.type = "boolean";
        if (data.defaultValue == "False"){
          this.defaultValue = false;
        } else if (data.defaultValue == "True") {
          this.defaultValue = true;
        } else {
          console.log("This parameter shouldn't be a boolean.")
        }
      } else if(data.type === "NoneType"){
        this.type = "null";
        this.defaultValue = null
      } else {
        this.type = data.type;
        this.defaultValue = data.defaultValue
      }
      console.log("name : ", this.name);
      console.log("defaultValue :", this.defaultValue)
      console.log("type :", this.type)
      console.log("length :", this.length)

    }
  }
  