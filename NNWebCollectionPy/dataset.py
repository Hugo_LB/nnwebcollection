import inspect
from flask_restful import Resource
from torchvision import datasets as tv
from torchtext import datasets as tt
from torchaudio import datasets as ta
import torch.optim as opt
from inspect import getmembers, isclass, isfunction, signature

"""
tuple_dataset_vision = getmembers(tv, isclass)
list_dataset = list(map(list, tuple_dataset_vision))
print([sublist[0] for sublist in list_dataset])


#torchtext dataset sous forme de fonction
tuple_dataset_text = getmembers(tt, isfunction)
list_dataset = list(map(list, tuple_dataset_text))
text_datasets = [sublist[0] for sublist in list_dataset]
dict_datasets = []
for dataset in text_datasets:
    dict_dataset_field = ["name", "category"]
    dict_dataset_value = [dataset, "Text"]
    dict_dataset = dict(zip(dict_dataset_field,dict_dataset_value))
    dict_datasets.append(dict_dataset)
print(dict_datasets)


tuple_dataset_audio = getmembers(ta, isclass)
list_dataset = list(map(list, tuple_dataset_audio))
print([sublist[0] for sublist in list_dataset])

method_to_call = getattr(tv, tuple_dataset_vision[0][0])
print(method_to_call)"""

#Format : {"name": "blabla", "category": "blabla"}, {"name": "blabla", "category": "blabla"}, etc


def get():
    visionDatasets = getVisionDatasets()
    textDatasets = getTextDatasets()
    audioDatasets = getAudioDatasets()
    optimizers = getOptimizers()
    datasets = visionDatasets + textDatasets + audioDatasets
    json = {'datasets': datasets, "optimizers": optimizers}
    print(optimizers)
    return json



def getVisionDatasets():
    tuple_dataset_vision = getmembers(tv, isclass)
    list_dataset = list(map(list, tuple_dataset_vision))
    visionDatasets = [sublist[0] for sublist in list_dataset]
    dict_datasets = []
    for dataset in visionDatasets:
        dict_dataset_field = ["name", "category"]
        dict_dataset_value = [dataset, "Vision"]
        dict_dataset = dict(zip(dict_dataset_field,dict_dataset_value))
        dict_datasets.append(dict_dataset)
    return dict_datasets

def getTextDatasets():
    tuple_dataset_text = getmembers(tt, isfunction)
    list_dataset = list(map(list, tuple_dataset_text))
    text_datasets = [sublist[0] for sublist in list_dataset]
    dict_datasets = []
    for dataset in text_datasets:
        dict_dataset_field = ["name", "category"]
        dict_dataset_value = [dataset, "Text"]
        dict_dataset = dict(zip(dict_dataset_field,dict_dataset_value))
        dict_datasets.append(dict_dataset)
    return dict_datasets

def getAudioDatasets():
    tuple_dataset_audio = getmembers(ta, isclass)
    list_dataset = list(map(list, tuple_dataset_audio))
    audioDatasets = [sublist[0] for sublist in list_dataset]
    dict_datasets = []
    for dataset in audioDatasets:
        dict_dataset_field = ["name", "category"]
        dict_dataset_value = [dataset, "Audio"]
        dict_dataset = dict(zip(dict_dataset_field,dict_dataset_value))
        dict_datasets.append(dict_dataset)
    return dict_datasets

def getOptimizers():
    tuple_optimizer = getmembers(opt, isclass)
    list_optimizer = list(map(list, tuple_optimizer))
    optimizers = [sublist[0] for sublist in list_optimizer]
    dict_optimizers = []
    for optimizer in optimizers:
        dict_optimizer_field = ["name", "parameters"]

        optimizer_signature = signature(getattr(opt, optimizer))
        parameters_keys = list(optimizer_signature.parameters.keys())[1:]
        parameters_values = [None if item.default==inspect._empty else item.default for item in list(optimizer_signature.parameters.values())[1:]]
        parameters_types = [None if item.default==inspect._empty else str(type(item.default).__name__) for item in list(optimizer_signature.parameters.values())[1:]]
        parameters_lengths = [len(item.default) if hasattr(item.default, '__len__') else 1 for item in list(optimizer_signature.parameters.values())[1:]]
        dict_parameters = []
        parameters = list(zip(parameters_keys, parameters_values, parameters_types, parameters_lengths))
        for parameter in parameters:
            dict_parameter_field = ["name", "defaultValue", "type", "length"]
            parameter_key = parameter[0]
            parameter_value = parameter[1]
            parameter_type = parameter[2]
            parameter_length = parameter[3]
            dict_parameter_values = [parameter_key, parameter_value, parameter_type, parameter_length]
            dict_parameter = dict(zip(dict_parameter_field, dict_parameter_values))
            dict_parameters.append(dict_parameter)
        
        dict_optimizer_values = [optimizer, dict_parameters]
        dict_optimizer = dict(zip(dict_optimizer_field, dict_optimizer_values))
        dict_optimizers.append(dict_optimizer)
    return dict_optimizers

get()